# SR2-FlopBox-Boumahdi

Samy Boumahdi

## Commande pour compiler :
	- mvn clean compile 
	

## Commande pour lancer :
	- mvn exec:java


## Commande Curl pour ajouter un couple 
	- curl -v -X POST "http://localhost:8080/myapp/server/create?name=test1&address=ftp.ubuntu.com"
	

## Commande Curl pour supprimer un couple
	- curl -v -X DELETE http://localhost:8080/myapp/server/name?name=test1


##Difficultés rencontrées :

Je n'ai jamais réussi à accéder via mon GET à un fichier ou répertoire ( curl -v -X GET http://localhost:8080/myapp/name/list?name=ftp.ubuntu.com&dir=/ voir à la fin de la vidéo)
J'ai l'impression que c'est un problème de connexion que malheureusement je n'ai pas réussi à régler.


