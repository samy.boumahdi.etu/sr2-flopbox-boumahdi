package sr.flopbox;

import org.apache.commons.net.ftp.FTPClient;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


/**
 * Chemin à la ressource server
 */
@Path("/server")
public class ServerResource {

    public static Map<String, FtpClient> servers = new HashMap<>();
    public FtpClient ftpClient;

    @POST
    @Path("/create")
    public String create(@QueryParam("name") String name, @QueryParam("address") String address) throws IOException {
        System.out.println(address);
        if (servers.containsKey(name)) {
            return "cle existe deja\n";
        } else {
            //FTPClient ftpClient = new FTPClient();
            this.ftpClient = new FtpClient(name);
            ftpClient.connect(address);
            servers.put(name, ftpClient);
            System.out.println("getname : " + servers.get(name));
            System.out.println("getAddress : " + servers.get(name).getRemoteAddress().toString());
            System.out.println(servers);
            return "Ajout du couple dans la hashmap\n";
        }
    }

    /*@PUT
    public void updateServer(String name, String address) throws IOException {
        FTPClient ftpClient = new FTPClient();
        ftpClient.connect(address);
        servers.put(name, ftpClient);
    }*/

    @DELETE
    @Path("/name")
    public void removeServer(@QueryParam("name") String name) {
        servers.remove(name);
    }


    @GET
    @Path("/name")
    @Produces(MediaType.TEXT_PLAIN)
    public FtpClient getServer(@QueryParam("name") String name) {
        System.out.println("name = " + name);
        String t = "server " + name + ", address : " + servers.get(name).getRemoteAddress().toString();
        System.out.println("t :::: " + t);
        return servers.get(name);
    }



    @GET
    @Path("/name/list")
    @Produces(MediaType.TEXT_PLAIN)
    public Serializable listDir(@PathParam("name") String name, @QueryParam("dir") String dir) throws IOException {
        if(!(this.servers.containsKey(name)))
            return "Ce serveur n'est pas dans la hashmap\n";
        FtpClient f = getServer(name);
        return f.listFiles(dir);
    }

}