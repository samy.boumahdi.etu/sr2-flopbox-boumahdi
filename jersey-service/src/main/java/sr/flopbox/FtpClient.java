package sr.flopbox;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import java.io.IOException;

public class FtpClient extends FTP {

    protected FTPClient client;
    private String server;
    private int port;
    private String username;
    private String password;
    private int response;

    public FtpClient(String name) {
        this.client = new FTPClient();
        this.server = name;
        this.port = 21;
        this.username = "anonymous";
        this.password = "anonymous@domain.com";
    }

    public FtpClient(String name, String username, String password) {

        this.client = new FTPClient();
        this.server = name;
        this.port = 21;
        this.username = username;
        this.password = password;
    }


    /**
     * fonction qui connecte le client au serveur
     */
    public void connectFTP(){
        boolean error = false;
        try {
            this.client.connect(this.server, this.port);
            this.response = this.client.getReplyCode();
            //refus connection
            if (!FTPReply.isPositiveCompletion(this.response)) {
                this.client.disconnect();
                System.err.println("FTP server refused connection.");
                System.exit(1);
            }
            client.logout();
        }catch(IOException e) {
            error = true;
            e.printStackTrace();
        } finally {
            if(client.isConnected()) {
                try {
                    client.disconnect();
                } catch(IOException ioe) {
                    // do nothing
                }
            }
            System.exit(error ? 1 : 0);
        }
    }

    /**
     * Fonction qui affiche retourne la list des fichiers et dossier, d'un dossier sur un serveur ftp
     * @param path String : fichier/repertoire a afficher
     * @return String : affichage du contenu du fichier/repertoire
     */

    public String listFiles(String path) throws IOException {
        this.connectFTP();
        String[] files  = this.client.listNames(path);
        String display;
        display = "Fichier:\n";
        for (String file : files) {
            display += file + "\n";
        }
        return display;
    }

}