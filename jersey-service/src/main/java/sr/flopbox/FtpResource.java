package sr.flopbox;

import org.apache.commons.net.ftp.FTPClient;

import javax.ws.rs.*;
import java.util.*;

@Path("/ftp")
public class FtpResource {

    private static FTPClient ftpClient = new FTPClient();
    private static Map<String, String> ftpMap = new HashMap<>();

    @POST
    @Path("/{name}")
    public void newFTP(@PathParam("name") String name, @FormParam("address") String address) {
        ftpMap.put(name, address);
    }
}
